from django.shortcuts import render

import models


# Create your views here.
def index(request):
    return render(request, 'index.html', {
        'random_host': models.Host.objects.exclude(room=None).exclude(machine=None).order_by('?')[0],
        'known_hosts': models.Host.objects.exclude(room=None).exclude(machine=None),
        'ghosts': models.Host.objects.filter(room=None).filter(machine=None)
    })
