from django.db import models
from django.conf import settings


# Create your models here.
class Host(models.Model):
    mac = models.CharField(
        help_text="The MAC address of the host.",
        primary_key=True,
        max_length=17,
        db_column='MAC',
    )

    ip = models.CharField(
        help_text="The current IP address of the host.",
        blank=True,
        max_length=15,
        db_column='IP',
    )

    room = models.PositiveSmallIntegerField(
        help_text="The room the host can be found in.",
        blank=True,
        db_column='ROOM',
    )

    machine = models.PositiveSmallIntegerField(
        help_text="The machine number of the host, local to the room.",
        blank=True,
        db_column='MACHINE',
    )

    last_updated = models.DateTimeField(
        help_text='The last time this record has been updated.',
        blank=False,
        db_column='LAST_UPDATED',
    )

    @property
    def hostname(self):
        return "{:0>2d}.{}.{}".format(
            self.machine, self.room, settings.DNS_DOMAIN)

    class Meta:
        managed = False
        db_table = 'hosts'
        ordering = ['room', 'machine']
