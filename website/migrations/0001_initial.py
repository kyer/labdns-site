# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Host',
            fields=[
                ('MAC', models.CharField(help_text=b'The MAC address of the host.', max_length=17, serialize=False, primary_key=True)),
                ('IP', models.CharField(help_text=b'The current IP address of the host.', max_length=15, blank=True)),
                ('ROOM', models.PositiveSmallIntegerField(help_text=b'The room the host can be found in.', blank=True)),
                ('MACHINE', models.PositiveSmallIntegerField(help_text=b'The machine number of the host, local to the room.', blank=True)),
            ],
            options={
                'db_table': 'hosts',
                'managed': False,
            },
            bases=(models.Model,),
        ),
    ]
